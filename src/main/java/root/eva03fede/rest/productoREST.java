/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva03fede.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.eva03fede.dao.ProductosJpaController;
import root.eva03fede.dao.exceptions.NonexistentEntityException;
import root.eva03fede.entity.Productos;

/**
 *
 * @author Feder
 */

@Path("producto")
public class productoREST {
    




@GET
@Produces(MediaType.APPLICATION_JSON)
public Response listaProductos (){
 
ProductosJpaController dao = new ProductosJpaController();

List<Productos> productos = dao.findProductosEntities();

return Response.ok(200).entity(productos).build();

}    
    
@POST
@Produces(MediaType.APPLICATION_JSON)
public Response agregar (Productos producto){

ProductosJpaController dao = new ProductosJpaController();


    try {
        dao.create(producto);
    } catch (Exception ex) {
        Logger.getLogger(productoREST.class.getName()).log(Level.SEVERE, null, ex);
    }

  return Response.ok(200).entity(producto).build();
  
}


@PUT
@Produces (MediaType.APPLICATION_JSON)

public Response actualizar (Productos producto){

 ProductosJpaController dao = new ProductosJpaController();
 
    try {
        dao.edit(producto);
    } catch (Exception ex) {
        Logger.getLogger(productoREST.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return Response.ok(200).entity(producto).build();

}


@DELETE
@Path ("/{ideliminar}")
@Produces (MediaType.APPLICATION_JSON)

public Response borrar (@PathParam("ideliminar") String ideliminar){

    ProductosJpaController dao = new ProductosJpaController();
    
    try {
        dao.destroy(ideliminar);
    } catch (NonexistentEntityException ex) {
        Logger.getLogger(productoREST.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return Response.ok(200).entity("producto eliminado").build();
    
}

@GET
@Path("/{idproducto}")
@Produces(MediaType.APPLICATION_JSON)

public Response buscar_producto(@PathParam("idproducto") String idproducto){

    ProductosJpaController dao = new ProductosJpaController();
    
    Productos producto = dao.findProductos(idproducto);
    
    return Response.ok(200).entity(producto).build();
    
}


    
}
